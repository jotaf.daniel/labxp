# História de Usuário

**COMO** _agente_

**QUERO** _ação_

**PARA QUE** _motivo_


---


# Testes

* [ ] _teste 1_
* [ ] _teste 2_


---


# Tarefas

* [ ] _tarefa 1_
* [ ] _tarefa 2_

/label ~feature
/milestone ...
/weight ...