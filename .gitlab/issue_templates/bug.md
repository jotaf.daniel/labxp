# Resumo 

(resumo breve do bug encontrado)


# Como reproduzir

(como reproduzir o erro -- muito importante)
* passo 1
* passo 2


# Qual o comportamento atual?

(o que está **de fato** acontecendo?)


# Qual o comportamento esperado?

(o que **deveria acontecer**?)


# Logs/Screenshots

(Cole logs relevantes aqui)

```
em caso de texto, cole aqui a saída
```


# Correções possíveis

(se for o caso, elenque uma alteração que possivelmente corrija)

/label ~bug
/weight ...
/milestone ...